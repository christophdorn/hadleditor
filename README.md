# README #

### What is this repository for? ###

Cloned from https://bitbucket.org/3m45t3r/dsl to allow updates to generated XML files and reformating of DSL "key words".

* Collaboration DSL
* 1.0.0-SNAPSHOT

### How do I get set up? ###

### 1. Prerequisites

You need to install:

* Eclipse Xtext http://www.eclipse.org/Xtext/download.html
* Java 1.5 or higher
* Maven, tested with 3.1.1 and 3.2.1(eclipse-embedded)

### 2. Setup

```
git clone https://bitbucket.org/3m45t3r/dsl.git
 cd dsl/main/
 mvn clean install
```

mvn install might not work if you use Xtext for the first time as the model workflow started via at.tuwien.dsg.dsl.GenerateDsl.mwe2 asks to download an antlr.jar
mvn install also might fail with insufficient memory.

Alternative to mvn install is to run GenerateDsl.mwe2 as MWE2 Workflow.

You also might have to add some src-gen folders to remove all resolving errors when importing the projects into eclipse.

Additionally Subproject at.tuwien.dsg.dsl requres following additional jars that haven't been integrated into the mvn pom yet:

* Apache Commons Lang 3 (e.g., commons-lang3-3.4.jar)
* Equinox Common 3.6 (e.g., org.eclipse.equinox.common-3.6.0.jar)

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner