package at.tuwien.dsg.generator;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import javax.inject.Inject;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IFileSystemAccessExtension3;
import org.eclipse.xtext.generator.InMemoryFileSystemAccess;
import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.eclipse.xtext.parser.IEncodingProvider;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.service.AbstractGenericModule;
import org.eclipse.xtext.util.StringInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.tuwien.dsg.DslStandaloneSetup;
import at.tuwien.dsg.dsl.Model;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class PlantUMLSequenceDiagramTest {

	@Inject
	private IParser parser;
	
	@Inject
	private XtextResourceSet resourceSet;
	
	private Model model;
	private String output = "";
	
	@Before
	public void setUp() throws Exception {
		String filepath = "D:\\Work\\Workspace\\runtime-EclipseXtext\\coretest\\src\\atc\\atc-rx.dsl";
		
		Injector injector = new DslStandaloneSetup().createInjectorAndDoEMFRegistration();
		injector.injectMembers(this);
		
		setupViaResourceSet(filepath);
	}

	@After
	public void tearDown() throws Exception {
		
		JavaIoFileSystemAccess fsa = new JavaIoFileSystemAccess();
		fsa.setOutputPath("D:\\Work\\Workspace\\BitBucket\\hADLeditor\\main\\at.tuwien.dsg.dsl\\src-test\\at\\tuwien\\dsg\\generator");
		Guice.createInjector(new AbstractGenericModule() {
			
			public Class<? extends IEncodingProvider> bindIEncodingProvider() {
				return IEncodingProvider.Runtime.class;
			}
			
		}).injectMembers(fsa);
		String filename = "testSequenceDiagram";
		fsa.generateFile(filename + ".puml.txt", output);
//		if (fsa instanceof IFileSystemAccessExtension3) {
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            new SourceStringReader(output).generateImage(out);
//            ((IFileSystemAccessExtension3)fsa).generateFile(filename + ".png", new ByteArrayInputStream(out.toByteArray()));
//        } else {
//            fsa.generateFile(filename + ".puml.txt", output);
//        }
	}
	
	
	private void setupViaResourceSet(String filepath) throws IOException
	{				
		Resource r = resourceSet.createResource(URI.createURI("dummy:/example.dsl"));
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		InputStream in = new FileInputStream(new File(filepath));		
		r.load(in, resourceSet.getLoadOptions());
		model = (Model) r.getContents().get(0);
	}
	
	private void setUpViaParser(String filepath)
	{
		try {			
			FileReader reader = new FileReader(filepath);					
			IParseResult result = parser.parse(reader);
			if(result.hasSyntaxErrors())
			{
				throw new ParseException("Provided input contains syntax errors.", 0);
			}			
			EObject r = result.getRootASTElement();
			//IFileSystemAccess fsa = new InMemoryFileSystemAccess();
			model = (Model)r;

		} catch (ParseException | IOException e) {			
			e.printStackTrace();
		}
	}

	@Test
	public void testSequenceDiagram() {

		assertNotNull(model);
		output = new PlantUMLSequenceDiagramGenerator().internalGenerate(model);
	}

}
