package at.tuwien.dsg.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.GenericXMLResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import at.tuwien.dsg.DslStandaloneSetup;

import com.google.inject.Injector;

public class DSL2XML {

	@Inject
	private XtextResourceSet resourceSet;
	
	public static void main(String[] args) {
		try {
			new DSL2XML().persist();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private DSL2XML(){}
	
	private void persist() throws IOException
	{
		String basis = "D:\\Work\\Workspace\\runtime-EclipseXtext\\coretest\\src";
		//String filepath = basis+"\\atc\\atc-techlevel.dsl";
		//String filepath = basis+"\\test1\\flowpatterns\\flowpatterns.dsl";
		String basis2 = "D:/Work/Workspace/VisualStudioGIT/hADL_LJ2sim/CADmodel/src";
		String filepath = basis2+"/patterns/artifact-loop-mapping.dsl";
		//String filepath = basis+"\\dispatch\\dispatch.dsl";

		Injector injector = new DslStandaloneSetup().createInjectorAndDoEMFRegistration();
		injector.injectMembers(this);

		Resource r = resourceSet.createResource(URI.createURI("dummy:/example.dsl"));
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		InputStream in = new FileInputStream(new File(filepath));		
		r.load(in, resourceSet.getLoadOptions());
		
		persistToXMLaccordingToXSD(r);
	}
	
	private void persistToXMI(Resource r) throws IOException
	{			
		// now write to XML/XMI:
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	    Map<String, Object> m = reg.getExtensionToFactoryMap();
	    m.put("dsl", new XMIResourceFactoryImpl());
	    
	    ResourceSet resSet2 = new ResourceSetImpl();
	    Resource rOut = resSet2.createResource(URI.createURI("dispatch.dsl"));
	    rOut.getContents().add(r.getContents().get(0));
	    rOut.save(Collections.EMPTY_MAP);
		
	}
	
	private void persistToXMLaccordingToXSD(Resource r) throws IOException
	{			
		GenericXMLResourceFactoryImpl xmlF = new GenericXMLResourceFactoryImpl();
		Resource rOut = xmlF.createResource((URI.createURI("dispatchXML.dsl")));
		rOut.getContents().add(r.getContents().get(0));
	    rOut.save(Collections.EMPTY_MAP);
	}
	
}
