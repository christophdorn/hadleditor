grammar at.tuwien.dsg.Dsl with org.eclipse.xtext.xbase.Xbase

generate dsl "http://www.tuwien.at/dsg/Dsl"

//*********************** common ***********************
Model:
	('package' package=QualifiedName)?
	//importSection=XImportSection? --> works only for JVMtypes not other dsls
	('import' imports+=FQNWithWildcard)*
	('hADL' hadl = HadlModel)?
	('LittleJIL' lj = LittleJilModel)? 	
    ;

//DslModel:
//	HadlModel | LittleJilModel
//;

FQN:
	ID ('.' ID)*;

FQNWithWildcard:
	FQN '.*'?;

Cardinality:
	lowerBound=(NUMBERS|'*') '..' upperBound=(NUMBERS|'*')
;

terminal NUMBERS:
	('0'..'9')+
;
//*********************** hadl ***********************
HadlModel:	
	'Model name:' name=FQN '{' 
		(structures+=HadlStructure)+
	'}'
;

HadlStructure:
	'Structure name:' name=FQN '{' 
	 (elements+=HadlElement)+ 
	('Relations {' (relations+=Relation)+ '}')?	
	('Activity Scopes {' (activityScopes+=ActivityScope)+ '}')?
	'}'
;

HadlElement:
	ArchElement | CollabLink;

CollabLink:
	'Link [' (name=FQN ":")? collabActionEndpoint=[Action|FQN] '=>' objActionEndpoint=[Action|FQN] ']'
	;

ActivityScope:
	'Scope name: '(name=FQN)? 
		('Link refs [' (linkRefs+=[CollabLink|FQN])+ ']')?		
		('Collaborator and Object refs [' (objectRefs+=[ArchElement|FQN])+ ']')?
		'cardinality: ' cardinality=Cardinality	
;

ArchElement:
	type=ArchElementType
	name=FQN
	('Actions' (actions+=Action)+)?
	('Substructure referenceTo: ' substructure=SubStructure)?
	;
	
SubStructure:
	structureRef=[HadlStructure|FQN] '{'
	('isVirtual' isVirtual=Boolean)?
	('Wires' (wires+=Wire)+)?
	'}'
;

// substructure wires would typically not have a name as they exist only within super structure and need not be references from the outside
Wire:
	'[' ( name=FQN ':')? collabActionEndpoint=[Action|FQN] '=>' objActionEndpoint=[Action|FQN] ']' 
	;

ArchElementType:
	CollaboratorType | ObjectType
	;

CollaboratorType:
	ComponentType | ConnectorType
;

ComponentType:
	{ComponentType} 'Component name:'
;

ConnectorType:
	{ConnectorType} 'Connector name:'
;

ObjectType:
	MessageType | RequestType | StreamType | ArtifactType
	;

MessageType:
	{MessageType} 'Message name:'
;

RequestType:
	{RequestType} 'Request name:'
;

StreamType:
	{StreamType} 'Stream name:'
;

ArtifactType:
	{ArtifactType} 'Artifact name:'
;

Action:
	'[' name=FQN
	('primitives: [' (types+=Primitive)+ ']')?
	('cardinality: ' cardinality=Cardinality)?
	']';

enum Primitive:
	C | R | U | D;

Boolean:
	'true' | 'false'
;

Relation:
	'[' name=FQN ':' from=[ArchElement|FQN] type=RelationType to=[ArchElement|FQN] ']';

RelationType:
	Inherits | Contains | Depends | References
;

Inherits:
	{Inherits} 'is supertype of';

Contains:
	{Contains} 'contains';

Depends:
	{Depends} 'depends on';

References:
	{References} 'references';

//*********************** little-jil ***********************
LittleJilModel:	
	'Process name:' name=FQN
	'Root Steps {' (steps+=Step)+ 
	'}'	
;

Step:
	'Step name:' name=FQN 'of type' kind=StepType	
	('has Cardinality: ' cardinality=StepCardinality)?
	('Local Data: {' (interfaces+=StepInterface)+ '}')?
	('Local to Parent Data Bindings: {' (bindings+=Binding)+ '}')?
	('Child Steps: {' (substeps+=Step)+ '}')?
	('handles exceptions with: {' (exceptionHandlers+=ExceptionHandler)+ "}")?
	('throws: {' (stepExceptions+=StepException)+ "}")?;
// not sure what step exception is supposed to be generated into

StepCardinality:
  lowerBound=(NUMBERS) '..' upperBound=(NUMBERS|'*')
  (controller=CardinalityController)?
;

CardinalityController:
	ParameterController | ExpressionController
;

ParameterController:
	'for elements in:' ref=[StepInterface|ID]
;

ExpressionController:
	'when expression true: ' expr=XExpression 
;

ExceptionHandler:
	HadlTypedExceptionHandler | JavaTypedExceptionHandler
;

HadlTypedExceptionHandler:	
	'Handler name:' name=FQN
	'of hADLType:' type=[ArchElement|FQN]
	'with continuation action: ' action=HandlerType	;
	
JavaTypedExceptionHandler:
	'Handler name:' name=FQN
	'of jvmType:' type=JvmTypeReference
	'with continuation action:' action=HandlerType
	;
	
Binding:
	'[ ' left=[StepInterface] bindingType=BindingType right=[StepInterface] ' ]'
;	

BindingType:
	InBinding | OutBinding | InOutBinding
;	
	
InBinding:
	{InBinding} '<'
;

OutBinding:
	{OutBinding} '>'
;	

InOutBinding:
	{InOutBinding} '<>'
;
//Binding:
//	'[ ' (InBinding | OutBinding | InOutBinding) ' ]';

//InBinding:
//	left=[StepInterface] '<' right=[StepInterface];

//OutBinding:
//	left=ID '>' right=FQN;

//InOutBinding:
//	left=ID '<>' right=FQN;

// probably not needed
StepException:
	'[' name=FQN
	('handled by' handeldBy=[ExceptionHandler|FQN])?
	']';

StepInterface:	
	HadlTypedStepInterface | JavaTypedStepInterface | ActionTypedStepInterface
;

HadlTypedStepInterface:
	'[ ' name=ValidID 'as'	
	 kind=InterfaceType
	'of hADLType' type=[ArchElement|FQN]
	']';
	
JavaTypedStepInterface:
	'[ ' name=ValidID 'as' 
	 kind=InterfaceType
	'of jvmType' type=JvmTypeReference
	']';
	
ActionTypedStepInterface:
	'[ ' name=ValidID 'as'	
	 kind=InterfaceType
	'of actionType' type=[Action|FQN]
	']';
	
enum HandlerType:
	Complete | Continue | Rethrow | Restart;

enum InterfaceType:
	Resource | ParameterIn | ParameterOut | ParameterInOut | Exception | Subtree;

enum StepType:
	Sequential | Try | Choice | Parallel | Leaf | TaskReference;