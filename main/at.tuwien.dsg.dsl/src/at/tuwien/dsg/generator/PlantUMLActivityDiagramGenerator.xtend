package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.LittleJilModel
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepType
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.HadlTypedStepInterface
import at.tuwien.dsg.dsl.JavaTypedStepInterface
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.HadlStructure
import org.eclipse.emf.ecore.util.EcoreUtil.Copier
import org.eclipse.emf.ecore.EObject
import at.tuwien.dsg.dsl.StepInterface
import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ActionTypedStepInterface

/**
 * hadl generator
 */
class PlantUMLActivityDiagramGenerator implements IGenerator {
	var String littleJILmodelId
	var String hADLmodelId
	var String packageName
	var boolean doIncludeLanes = false

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				packageName = m.package
			} else {
				packageName = m.hashCode.toString
			}
			if (m.hadl != null)
				hADLmodelId = m.hadl.name
			if (m.lj != null)
				littleJILmodelId = m.lj.name
			fsa.generateFile(packageName + "-activitydiagram.plantuml.txt", m.compile)
		}

	}

	def String internalGenerate(Model m, boolean includeLanes)
	{
		doIncludeLanes = includeLanes;
		if (m.package != null && !m.package.empty) {
			packageName = m.package
		} else {
			packageName = m.hashCode.toString
		}
		if (m.hadl != null)
				hADLmodelId = m.hadl.name
		if (m.lj != null)
				littleJILmodelId = m.lj.name
		m.compile.toString
	}

	def compile(Model m) '''
		«IF m.lj != null»					
			@startuml
			«FOR step : m.lj.steps»					
			«IF doIncludeLanes»|Process Trigger|«ENDIF»
			start
			«step.compile(null)»
			«IF doIncludeLanes»|Process Trigger|«ENDIF»
			stop
			«ENDFOR»
			@enduml
		«ENDIF»
	'''

	// PARALLEL FORKs are placed in the lane of the first subactivity's agent
	// TRY CONSTRUCT needs exception handler
	// CHOICE AND TRY constructs are all drawn in parent lane, and very ugly layouted
	def compile(Step step, Step parent) '''	
		«IF !step.hasAgent && parent == null»
			legend 
			ERROR: Step «step.name» has no agent and parent == null
			endlegend
		«ELSEIF !step.hasAgent»
			«step.initAgentFromParent(parent)»
		«ENDIF»	
		«IF step.cardinality != null»
			while (unfulfilled cardinality?) is («step.cardinality.lowerBound»..«step.cardinality.upperBound»)  
		«ENDIF»
		«IF doIncludeLanes»| «step.resourceName» |«ENDIF»
		«IF step.kind == StepType.LEAF»			
			: «step.name» ;	
		«ELSEIF step.kind == StepType.SEQUENTIAL»
			«FOR substep : step.substeps»
				«compile(substep, step)»
			«ENDFOR»
		«ELSEIF step.kind == StepType.CHOICE»			
			«FOR substep : step.substeps BEFORE ' if (applicable) then (yes) \n' SEPARATOR '\n elseif (applicable) then (yes)' AFTER ' else (nothing applicable) \n endif'»
				«compile(substep, step)»
			«ENDFOR»	
		«ELSEIF step.kind == StepType.PARALLEL»						
			«FOR substep : step.substeps BEFORE 'fork \n' SEPARATOR '\n fork again' AFTER 'end fork'»
				«compile(substep, step)»
			«ENDFOR»	
		«ELSEIF step.kind == StepType.TRY»			
			repeat
			«FOR substep : step.substeps BEFORE ' if (tried) then (not yet) \n' SEPARATOR '\n elseif (tried) then (not yet)' AFTER ' else (all failed) \n endif \n'»
				«compile(substep, step)»
			«ENDFOR»
			repeat while (unsuccessful and choices left?)	
		«ENDIF»
		«IF step.cardinality != null»
			endwhile (completed or opted-out)
		«ENDIF»	
	'''
	

	def boolean hasAgent(Step step)
	{
		if (step != null)
		{	
			for (intf : step.interfaces)
			{
				if (intf.kind == InterfaceType.RESOURCE && intf.name.equals("agent")) 
					return true; // found
			}
		}
		return false
	}

	def void initAgentFromParent(Step step, Step parentStep) 
	{		 
		if (step != null && parentStep != null)
		{
			for (intf : parentStep.interfaces)
			{
				if (intf.kind == InterfaceType.RESOURCE && intf.name.equals("agent"))
				{ 
					var copier = new Copier()
 					var result = copier.copy(intf)  
  					copier.copyReferences()
					step.interfaces.add(result as StepInterface)
					return		
				}
			}		
		}
		System.out.println(String.format("Couldn't initialize step %s agent from parent %s",step.name, parentStep.name))					
	}

	def String getResourceName(Step step)
	{
		if (step == null)
		{
			System.err.println("Provided Step == null")
			return "NULL"		
		}
		for (intf : step.interfaces)
		{
			if (intf.kind == InterfaceType.RESOURCE && intf.name.equals("agent")) 
			{
				if (intf instanceof HadlTypedStepInterface && (intf as HadlTypedStepInterface).type != null) {
					return id((intf as HadlTypedStepInterface).type)			
				} else if (intf instanceof JavaTypedStepInterface) {
					return (intf as JavaTypedStepInterface).type.qualifiedName
				} else if (intf instanceof ActionTypedStepInterface) {
					return id((intf as ActionTypedStepInterface).type)
				}				
			}
		}
		System.out.println("No Agent for Step found: "+step.name)
		return "ANONYMOUSNULL"
	}

	
	def dispatch String id(HadlModel o) {
		hADLmodelId	
	}

	def dispatch String id(ArchElement o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(HadlStructure o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String id(Action o) {
		id(o.eContainer)+"."+o.name
	}
}
