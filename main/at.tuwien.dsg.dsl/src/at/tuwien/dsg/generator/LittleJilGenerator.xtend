package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.Binding
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.ExceptionHandler
import at.tuwien.dsg.dsl.HadlTypedExceptionHandler
import at.tuwien.dsg.dsl.HadlTypedStepInterface
import at.tuwien.dsg.dsl.InBinding
import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.JavaTypedExceptionHandler
import at.tuwien.dsg.dsl.JavaTypedStepInterface
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.RequestType
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepException
import at.tuwien.dsg.dsl.StepInterface
import at.tuwien.dsg.dsl.StepType
import at.tuwien.dsg.dsl.StreamType
import at.tuwien.dsg.dsl.impl.InOutBindingImpl
import at.tuwien.dsg.dsl.impl.OutBindingImpl
import at.tuwien.dsg.generator.util.IdProvider
import at.tuwien.dsg.generator.util.ImplicitAgentBindingFactory
import at.tuwien.dsg.generator.util.ImplicitInterfaceFactory
import at.tuwien.dsg.generator.util.LittleJilExceptionPropagator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.LittleJilModel
import at.tuwien.dsg.dsl.Cardinality
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.StepCardinality
import at.tuwien.dsg.dsl.ExpressionController
import at.tuwien.dsg.dsl.ParameterController
import org.apache.commons.lang3.StringEscapeUtils
import at.tuwien.dsg.dsl.OutBinding
import at.tuwien.dsg.dsl.InOutBinding
import at.tuwien.dsg.dsl.ActionTypedStepInterface
import at.tuwien.dsg.dsl.Action

/**
 * LittleJil generator
 */
class LittleJilGenerator implements IGenerator {

	var String littleJILmodelId
	var String hADLmodelId
	var String packageName
	
	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				packageName = m.package
			} else {
				packageName = m.hashCode.toString
			}
			if (m.hadl != null)
				hADLmodelId = m.hadl.name
			if (m.lj != null)
				littleJILmodelId = m.lj.name
			fsa.generateFile(packageName + "-littlejil.xml", m.compile)					
		}
	}

	def String internalGenerate(Model m)
	{
		if (m.package != null && !m.package.empty) {
			packageName = m.package
		} else {
			packageName = m.hashCode.toString
		}
		if (m.hadl != null)
				hADLmodelId = m.hadl.name
		if (m.lj != null)
				littleJILmodelId = m.lj.name
		m.compile.toString
	}

	def compile(Model m) '''
		«ImplicitInterfaceFactory.clearBindingTypeMap»
«««		«FOR model : m.models»
«««			«IF model instanceof LittleJilModel»
«««				«model.compile»
«««			«ENDIF»
«««		«ENDFOR»
		«IF m.lj != null»
			«m.lj.compile»
		«ENDIF»
	'''

	def compile(LittleJilModel lm) '''
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE littlejil PUBLIC "-//LASER//DTD Little-JIL 1.5//EN" "http://laser.cs.umass.edu/dtd/littlejil-1.5.dtd">
<littlejil>
	<module>
		«FOR s : lm.steps»
			«s.compile»
		«ENDFOR»
	</module>
</littlejil>
	'''

	def compile(Binding b) '''
		<binding name-in-child="«b.left.name»">
				<scope-binding kind="«bindingKind(b.bindingType)»" name-in-parent="«b.right.name»" />
		</binding>
	'''

	def compile(Step s) '''
«««		«ImplicitInterfaceFactory.createInterfaces(s)» // CHECK WHAT TO REACTIVATE OF THESES METHODS
«««		«ImplicitAgentBindingFactory.createAgentBinding(s)»		
«««		«LittleJilExceptionPropagator.createImplicitException(s)»
		«FOR b : s.bindings»
			«b.compile»
		«ENDFOR»
		«IF s.kind != StepType.TASK_REFERENCE»
			<step-declaration id="«IdProvider.getLitJilId»" kind="«s.kind.toString.toLowerCase»" name="«s.name»">
				«FOR si : s.interfaces»
					«si.compile»
				«ENDFOR»
				«FOR ex : s.stepExceptions»
					«ex.compile»
				«ENDFOR»
				«IF !s.substeps.empty»
					«FOR su : s.substeps»
						<connector id="«IdProvider.getLitJilId»">
							«IF su.cardinality == null»
								<substep-connector />
							«ELSE»
								<substep-connector>
									<cardinality lower-bound="«su.cardinality.lowerBound»" upper-bound="«upperBound(su.cardinality)»" />
									«IF su.cardinality.controller != null»«su.cardinality.controller»«ENDIF» 									
								</substep-connector>
							«ENDIF»
							«su.compile»
						</connector>
					«ENDFOR»
				«ENDIF»
				«FOR exH : s.exceptionHandlers»
					«exH.compile»
				«ENDFOR»
			</step-declaration>
		«ELSE»
			<step-reference id="«IdProvider.getLitJilId»" target="«s.name»"/>
		«ENDIF»
	'''

	def compile(StepException ex) '''
		<interface-decl kind="exception" name="«ex.name»">
			<external-object encoding="odesc">
				<aggregate type="«exceptionHandlerType(ex.handeldBy)»" />
			</external-object>
		</interface-decl>
	'''

	def compile(ExceptionHandler eh) '''
		<connector id="«IdProvider.getLitJilId»">
		<handler-connector continuation-action="«eh.action.toString.toLowerCase»" parameter-name="«eh.name»">
			<external-object encoding="odesc">
			       <aggregate type="«exceptionHandlerType(eh)»"/>
			   </external-object>
		</handler-connector>
		</connector>
	'''

	def compile(StepInterface si) '''
		<interface-decl kind="«interfaceKind(si.kind)»" name="«si.name»">
			<external-object encoding="odesc">
				<aggregate type="«stepInterfaceType(si)»" />
			</external-object>
		</interface-decl>
	'''

	/** upper bound * is in fact the value 2147483647 in little-jil */
	def String upperBound(StepCardinality c) {
		if ('*'.equals(c.upperBound)) {
			"2147483647"
		} else {
			c.upperBound
		}
	}

	def String interfaceKind(InterfaceType ityp) {
		switch ityp {
			case SUBTREE: "local-parameter"
			case PARAMETER_IN: "in-parameter"
			case PARAMETER_OUT: "out-parameter"
			case PARAMETER_IN_OUT: "in-out-parameter"
			case RESOURCE: "resource"
			case EXCEPTION: "exception"
			default: ""
		}
	}

	def String exceptionHandlerType(ExceptionHandler eh) {
		if (eh instanceof HadlTypedExceptionHandler) {
			return archElementType((eh as HadlTypedExceptionHandler).type.type)
		} else if (eh instanceof JavaTypedExceptionHandler) {
			return (eh as JavaTypedExceptionHandler).type.qualifiedName
		}
	}

	def String stepInterfaceType(StepInterface si) {
		if (si instanceof HadlTypedStepInterface) {
			return id((si as HadlTypedStepInterface).type)
			// We want the FQN of the hADL element not just the type, as this would be ambiguous as to who exactly would execute the step
			//return archElementType((si as HadlTypedStepInterface).type.type)
		} else if (si instanceof JavaTypedStepInterface) {
			return (si as JavaTypedStepInterface).type.qualifiedName
		} else if (si instanceof ActionTypedStepInterface) {
			return id((si as ActionTypedStepInterface).type)
		}
		
	}

	def dispatch String archElementType(ComponentType t) {
		"Component"
	}

	def dispatch String archElementType(ConnectorType t) {
		"Connector"
	}

	def dispatch String archElementType(ArtifactType t) {
		"Artifact"
	}

	def dispatch String archElementType(MessageType t) {
		"Message"
	}

	def dispatch String archElementType(RequestType t) {
		"Request"
	}

	def dispatch String archElementType(StreamType t) {
		"Stream"
	}

	def dispatch String bindingKind(InBinding b) {
		"copy-in"
	}

	def dispatch String bindingKind(OutBinding b) {
		"copy-out"
	}

	def dispatch String bindingKind(InOutBinding b) {
		"copy-in-and-out"
	}
	
	def dispatch String id(HadlModel o) {
		hADLmodelId	
	}

	def dispatch String id(ArchElement o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(HadlStructure o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String id(Action o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String controller(ParameterController pc) {
		"<control-parameter parameter-name=\""+pc.ref+"\" />"
	}

	def dispatch String controller(ExpressionController ec) {
		"<predicate-expression expression=\""+StringEscapeUtils.escapeXml11(ec.expr.toString)+"\" />"
	}

}
