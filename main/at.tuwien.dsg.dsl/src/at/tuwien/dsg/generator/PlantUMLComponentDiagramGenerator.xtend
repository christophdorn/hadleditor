package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ActivityScope
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.Contains
import at.tuwien.dsg.dsl.Depends
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.Inherits
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.ObjectType
import at.tuwien.dsg.dsl.Primitive
import at.tuwien.dsg.dsl.References
import at.tuwien.dsg.dsl.Relation
import at.tuwien.dsg.dsl.RequestType
import at.tuwien.dsg.dsl.StreamType
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.Wire
import at.tuwien.dsg.dsl.SubStructure
import at.tuwien.dsg.dsl.CollaboratorType

/**
 * hadl generator
 */
class PlantUMLComponentDiagramGenerator implements IGenerator {
	var String hADLmodelId
	var String packageName
	var boolean doCompact = false

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				packageName = m.package
			} else {
				packageName = m.hashCode.toString
			}
			if (m.hadl != null)
				hADLmodelId = m.hadl.name
			fsa.generateFile(packageName + "-componentdiagram.plantuml.txt", m.compile)
		}

	}

	def String internalGenerate(Model m, boolean doCompactPresentation)
	{
		doCompact = doCompactPresentation
		if (m.package != null && !m.package.empty) {
			packageName = m.package
		} else {
			packageName = m.hashCode.toString
		}
		if (m.hadl != null)
			hADLmodelId = m.hadl.name
		m.compile.toString
	}

	def compile(Model m) '''
		«IF m.hadl != null»			
			«m.hadl.compile»
		«ENDIF»
	'''

	def compile(HadlModel hm) '''
		@startuml
		skinparam componentStyle uml2
			«FOR hs : hm.structures»
				«hs.compile»
			«ENDFOR»
			«FOR hs : hm.structures»
				«FOR e : hs.elements»
					«IF e instanceof ArchElement»
						«(e as ArchElement).compileSub»
					«ENDIF»						
				«ENDFOR»
			«ENDFOR»
		hide empty members
		@enduml
	'''

	def compile(HadlStructure hs) '''
		
		package "«hs.name»" {								
				«FOR e : hs.elements»
					«IF e instanceof ArchElement»
						«(e as ArchElement).compile»
					«ELSEIF e instanceof CollabLink»
						«(e as CollabLink).compile»
					«ENDIF»
				«ENDFOR»
				«FOR r : hs.relations»
					«r.compile»
				«ENDFOR»				
		}
	'''



	def compile(CollabLink l) '''				
		«val objAE = l.objActionEndpoint»
		«var collAE = l.collabActionEndpoint»		
		«IF (objAE != null && collAE != null)»
			«IF doCompact»
				«id((l.collabActionEndpoint.eContainer as ArchElement))» -( «id(objAE)» «IF l.name != null»	: «l.name»	«ENDIF»
			«ELSE»
				«id(collAE)» -- «id(objAE)» «IF l.name != null»	: «l.name»	«ENDIF»
			«ENDIF»
		«ENDIF»			
	'''

	def compile(ArchElement ae) '''		
		«val isProvided = isProvided(ae.type)»
		component «ae.name» as «id(ae)» «color(ae.type)»								
		«IF !(doCompact && !isProvided)»
			«FOR a : ae.actions»			
				«a.compile(isProvided, id(ae))»			
			«ENDFOR»
		«ENDIF»
	'''

	def compileSub(ArchElement ae) '''				
		«IF ae.substructure != null»
			«ae.substructure.compile(id(ae))»
		«ENDIF»		
	'''
	

	def compile(Action a, boolean isProvided, String source) '''		
		interface "«a.name» <«FOR p : a.types SEPARATOR ' '»«primitive(p)»«ENDFOR»>«IF a.cardinality != null»[«a.cardinality.lowerBound»..«a.cardinality.upperBound»]«ENDIF»" as «id(a)»
		«IF isProvided»  
			«source» -up- «id(a)»
		«ELSE»
			«source» -down-( «id(a)»
		«ENDIF»	
	'''


	def compile(Relation r) '''
		«IF (r.from != null && r.to != null)»
			«id(r.from)» «relationType(r.type)» «id(r.to)» : «r.name»
		«ENDIF»	
	'''

	def compile(SubStructure sub, String parent) '''		
		«parent» -[#black]-> «sub.structureRef.name» : substructure
		«IF !doCompact»	
			«FOR w : sub.wires»
			«w.compile»
			«ENDFOR»
		«ENDIF»						
	'''

	def compile(Wire w) '''
		«val objAE = w.objActionEndpoint»
		«val collAE = w.collabActionEndpoint»					
		«IF (objAE != null && collAE != null)»
			«id(collAE)» -[#black]-> «id(objAE)» «IF w.name != null» : «w.name»	«ENDIF»
		«ENDIF»			
	'''

	def dispatch color(ComponentType t) { "#GreenYellow " }
	
	def dispatch color(ConnectorType t) { "#Green" }
	
	def dispatch color(MessageType t) { "#Yellow" }
	
	def dispatch color(StreamType t) { "#Coral" }
	
	def dispatch color(ArtifactType t) { "#DarkOrange" }
	
	def dispatch color(RequestType t) { "#DarkSalmon" }

	def String primitive(Primitive p) {
		switch p {
			case C: 'C'
			case R: 'R'
			case U: 'U'
			case D: 'D'
			default: ""
		}
	}

	def dispatch String relationType(Inherits r) {
		" <|.[#blue]. " // TO_INHERITS_FROM
	}

	def dispatch String relationType(Contains r) {
		" *.[#blue]." // FROM_CONTAINS_TO
	}

	def dispatch String relationType(Depends r) {
		" *.[#blue].> " //FROM_DEPENDS_ON_TO
	}

	def dispatch String relationType(References r) {
		".[#blue].>" //REFERENCES
	}

	
	def dispatch boolean isProvided(CollaboratorType ct){
		false
	}
	
	def dispatch boolean isProvided(ObjectType ct){
		true
	}		
	
	def dispatch String id(HadlModel o) {
		hADLmodelId // check whether we need to include the package name into a FQN	
	}

	def dispatch String id(Action o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(ArchElement o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(CollabLink o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(HadlStructure o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(Wire o) {
		id(o.eContainer)+"."+o.name
	}
}
