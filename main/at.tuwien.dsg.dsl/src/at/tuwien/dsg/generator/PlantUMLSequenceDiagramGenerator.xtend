package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.LittleJilModel
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepType
import at.tuwien.dsg.dsl.impl.DslFactoryImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.HadlTypedStepInterface
import at.tuwien.dsg.dsl.JavaTypedStepInterface
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ActionTypedStepInterface

/**
 * hadl generator
 */
class PlantUMLSequenceDiagramGenerator implements IGenerator {
	var String littleJILmodelId
	var String hADLmodelId
	var String packageName

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				packageName = m.package
			} else {
				packageName = m.hashCode.toString
			}
			if (m.hadl != null)
				hADLmodelId = m.hadl.name
			if (m.lj != null)
				littleJILmodelId = m.lj.name
			fsa.generateFile(packageName + "-sequencediagram.plantuml.txt", m.compile)
		}

	}

	def String internalGenerate(Model m)
	{
		if (m.package != null && !m.package.empty) {
			packageName = m.package
		} else {
			packageName = m.hashCode.toString
		}
		if (m.hadl != null)
				hADLmodelId = m.hadl.name
		if (m.lj != null)
				littleJILmodelId = m.lj.name
		m.compile.toString
	}

	def compile(Model m) '''
«««		«FOR model : m.models»
«««			«IF model instanceof LittleJilModel»
«««				«toSequenceDiagram(model, m.package).toString»
«««			«ENDIF»
«««		«ENDFOR»	
		«IF m.lj != null»					
			«toSequenceDiagram(m.lj, m.package).toString»
		«ENDIF»
	'''

	def StringBuffer toSequenceDiagram(LittleJilModel lm, String packageName)
	{
		val umlText = new StringBuffer()
		var title = ""
		var exeResource = "Anonymous"
		umlText.append("@startuml\n");
	//	umlText.append("!pragma teoz true \n");
			title = packageName			
			for (rootStep :  lm.steps)
			{ // create a sequence diagram for each root step, for now only assume one root step
				var Step lastStep = null
				var scope = rootStep.name
				umlText.append("== "+ scope + " ==\n")
				for (intf : rootStep.interfaces)
				{
					if (intf.kind == InterfaceType.RESOURCE) // for now take name, (otherwise would be "agent" everytime)
						exeResource = intf.name;
				}				
				if (rootStep.getKind() == StepType.PARALLEL)
				{					
					for (step : rootStep.substeps)
					{
						umlText.append("group Parallel: "+step.name+" \n")
						processStep(umlText, step, exeResource,lastStep)
						umlText.append("end \n")					
					}
				}
				else if (rootStep.getKind() == StepType.SEQUENTIAL)
				{
					for (step : rootStep.substeps)
					{						
						lastStep = processStep(umlText, step, exeResource, lastStep)									
					}
				}
				else if (rootStep.getKind() == StepType.CHOICE)
				{
					var count = 1
					var total = rootStep.substeps.size
					for (step : rootStep.substeps)
					{						
						if (count == 1)
						{
							umlText.append("alt condition "+rootStep.name +"\n")
							lastStep = processStep(umlText, step, exeResource, lastStep)							
							count = count+1
							if (count == total)
								umlText.append("end \n")
						}
						else
						{
							umlText.append("else condition "+ count +"\n")
							lastStep = processStep(umlText, step, exeResource, lastStep)							
							count = count+1
							if (count == total)
								umlText.append("end \n")
						}														
					}
				} 
				else if (rootStep.getKind() == StepType.TRY)
				{					
					for (step : rootStep.substeps)
					{
						umlText.append("group try: "+step.name+" \n")
						processStep(umlText, step, exeResource,lastStep)
						umlText.append("end \n")					
					}
				}
											
		
		}
		umlText.append("@enduml\n");
		return umlText
	}
			
	def Step processStep(StringBuffer umlText, Step step, String parentAgentName, Step prevStep)
	{
		initResourceName(step, parentAgentName)		
		var exeResource = getResourceName(step)
		var lastStep = prevStep
		//var scope = step.name		
		if (step.getKind() == StepType.PARALLEL) {
			var count = 0			
			var total = step.substeps.size			
			for (substep : step.substeps) {
				if (count == 0) {				
					umlText.append("par "+step.name+"\n")
					processStep(umlText, substep, exeResource, lastStep)
					count = count + 1
					if (count == total) {
						umlText.append("end \n")
					}					
				} else {
					umlText.append("else \n")	
					processStep(umlText, substep, exeResource, lastStep)
					count = count + 1
					if (count >= total)
						umlText.append("end \n")
				}
			}
			return prevStep
		} else if (step.getKind() == StepType.SEQUENTIAL) {
			for (substep : step.substeps) {
				lastStep = processStep(umlText, substep, exeResource, lastStep)				
			}
			return prevStep
		} else if (step.getKind() == StepType.CHOICE) {
			var count = 0
			var total = step.substeps.size
			for (substep : step.substeps) {
				if (count == 0) {
					umlText.append("alt condition " + step.name + "\n")
					processStep(umlText, substep, exeResource, lastStep)
					count = count + 1
					if (count == total)
						umlText.append("end \n")
				} else {
					umlText.append("else condition " + count + "\n")
					processStep(umlText, substep, exeResource, lastStep)
					count = count + 1
					if (count >= total)
						umlText.append("end \n")
				}
			}
		} else if (step.getKind() == StepType.LEAF) {
			// now create actual link between actors/components
			if (lastStep != null) 
			{
				umlText.append(getResourceName(prevStep) +" -> "+ exeResource +" : "+ step.name +" \n")	
			}
			return step
		}
		else if (step.getKind() == StepType.TRY)
		{					
			for (substep : step.substeps)
			{
				umlText.append("group try: "+step.name+" \n")
				processStep(umlText, step, exeResource,lastStep)
				umlText.append("end \n")					
			}
			return step
		}			
		return lastStep
		// what step to return in the end, should be unambiguous by looking at the dataflow
	}
	
	def String getResourceName(Step step)
	{
		if (step == null)
			return "NULL"
		for (intf : step.interfaces)
		{
			if (intf.kind == InterfaceType.RESOURCE && intf.name.equals("agent")) // for now take name, (otherwise would be "agent" everytime)
			{
				if (intf instanceof HadlTypedStepInterface && (intf as HadlTypedStepInterface).type != null) {
					return id((intf as HadlTypedStepInterface).type)			
				} else if (intf instanceof JavaTypedStepInterface) {
					return (intf as JavaTypedStepInterface).type.qualifiedName
				} 	else if (intf instanceof ActionTypedStepInterface) {
					return id((intf as ActionTypedStepInterface).type)
				}				
			}
		}
		return "ANONYMOUSNULL"
	}
	
	def void initResourceName(Step step, String parentName) // TODO needs to be changed to copy parent interface type (hADL or JVM, not just generic resource and 'agent' as name)
	{
		if (step != null)
		{	
			for (intf : step.interfaces)
			{
				if (intf.kind == InterfaceType.RESOURCE && intf.name.equals("agent")) 
					return; // found
			}
			var intf = DslFactoryImpl.init().createStepInterface();
			intf.setKind(InterfaceType.RESOURCE);
			intf.setName(parentName);
			step.interfaces.add(intf);		
		}	
	}
	
	def dispatch String id(HadlModel o) {
		hADLmodelId	
	}

	def dispatch String id(ArchElement o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(HadlStructure o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String id(Action o) {
		id(o.eContainer)+"."+o.name
	}
}
