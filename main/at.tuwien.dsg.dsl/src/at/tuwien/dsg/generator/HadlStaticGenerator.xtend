package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ActivityScope
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.ObjectType

import at.tuwien.dsg.dsl.Relation
import at.tuwien.dsg.dsl.RequestType
import at.tuwien.dsg.dsl.StreamType
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.Wire
import java.util.Hashtable
import at.tuwien.dsg.dsl.ArchElementType

/**
 * hadl generator
 */
class HadlStaticGenerator implements IGenerator {
	var String modelId
	var String packageId
	
	// mapping FQN to shortest model unique name
	var Hashtable<String,String> shortNames = new Hashtable<String,String>()	
	
	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				packageId = m.package
			} else {
				packageId = m.hashCode.toString
			}
			if (m.hadl != null)
			{
				modelId = m.hadl.name
				m.compile(fsa)				
			}			
		}

	}

	def compile(Model m, IFileSystemAccess fsa) '''
		«IF m.hadl != null»
			«idGen(m.hadl)»
			«fsa.generateFile(modelId+".java", m.hadl.compile)»			
		«ENDIF»
	'''

	def String internalGenerate(Model m)
	{
		if (m.package != null && !m.package.empty) {
			packageId = m.package
		} else {
			packageId = m.hashCode.toString
		}
		if (m.hadl != null)
		{
			modelId = m.hadl.name
			idGen(m.hadl)
			m.hadl.compile.toString	
		}					
	}

	def compile(HadlModel hm) '''
		package «packageId»;		
		
		public class «hm.name» {			
		public static final String MODEL_«hm.name» = "«modelId»";
		
		«FOR hs : hm.structures»
		«hs.compile»
		«ENDFOR»						
		}
	'''



	def compile(HadlStructure hs) '''
		
		«var type = type(hs)»
		«var id = id(hs)»	
		public static final String «id2static(shortNames.get(type+"_"+id))» = "«id»";  
		
				«FOR e : hs.elements»
					«IF (e instanceof ArchElement)»
						«(e as ArchElement).compile»
					«ENDIF»
				«ENDFOR»				
				«FOR e : hs.elements»
					«IF e instanceof CollabLink»
						«(e as CollabLink).compile»
					«ENDIF»
				«ENDFOR»
				«FOR r : hs.relations»
					«r.compile»
				«ENDFOR»				
				«FOR c : hs.activityScopes»
					«c.compile»
				«ENDFOR»		
	'''

	def compile(ActivityScope ac) '''
		«var type = type(ac)»
		«var id = id(ac)»	
		public static final String «id2static(shortNames.get(type+"_"+id))» = "«id»";
	'''

	def compile(CollabLink l) '''
		«var type = type(l)»
		«var id = id(l)»	
		public static final String «id2static(shortNames.get(type+"_"+id))» = "«id»";
	'''



	def compile(ArchElement ae) '''
		
		«var type = type(ae.type)»
		«var id = id(ae)»	
		public static final String «id2static(shortNames.get(type+"_"+id))» = "«id»"; 
		«FOR a : ae.actions»
			«a.compile(ae.type, ae.name)»
		«ENDFOR»				
	'''

	def compile(Action a, ArchElementType pType, String parentName) '''
		«var type = type(a, pType, parentName)»		
		«var id = id(a)»	
		public static final String «id2static(shortNames.get(type+"_"+id))» = "«id»";
	'''

	def compile(Relation r) '''
		«var type = type(r)»
		«var id = id(r)»	
		public static final String «id2static(shortNames.get(type+"_"+id))» = "«id»";
	'''

	
	def idGen(HadlModel hm)
	{
		shortNames.put("hADL_"+hm.name, "hADL_"+hm.name);
		for (hs : hm.structures)
		{
			hs.idGen
		}		
	}
	
	def idGen(HadlStructure hs)
	{
		var type = type(hs)
		var id = id(hs)	
		if (shortNames.contains(type+"_"+hs.name))
			shortNames.put(type+"_"+id, type+"_"+id)
		else
			shortNames.put(type+"_"+id, type+"_"+hs.name)
		for (e : hs.elements)
		{
			if (e instanceof ArchElement)
				e.idGen
			else if (e instanceof CollabLink)
				e.idGen
		}
		for (r : hs.relations)
		{
			r.idGen
		}	
		for (s : hs.activityScopes)
		{
			s.idGen
		}
	}	
	
	def idGen(ArchElement ae)
	{
		var type = type(ae.type)
		var id = id(ae)	
		if (shortNames.contains(type+"_"+ae.name))
			shortNames.put(type+"_"+id, type+"_"+id)
		else
			shortNames.put(type+"_"+id, type+"_"+ae.name)
		for (a : ae.actions)
		{
			a.idGenAction(ae.type, ae.name)
		}
		// ignoring substructure wires for now	
	}
	
	def idGen(CollabLink ae)
	{
		var type = type(ae)	
		var id = id(ae)	
		if (shortNames.contains(type+"_"+ae.name))
			shortNames.put(type+"_"+id, type+"_"+id)
		else
			shortNames.put(type+"_"+id, type+"_"+ae.name)		
	}
		
	def idGenAction(Action a, ArchElementType pType, String parentName)
	{		
		var type = type(a, pType, parentName);		
		var id = id(a)		
		if (shortNames.contains(type+"_"+a.name))
			shortNames.put(type+"_"+id, type+"_"+id)
		else
			shortNames.put(type+"_"+id, type+"_"+a.name)		
	}
	
	def idGen(Relation r)
	{
		var type = type(r)
		var id = id(r)
		if (shortNames.contains(type+"_"+r.name))
			shortNames.put(type+"_"+id, type+"_"+id)
		else
			shortNames.put(type+"_"+id, type+"_"+r.name)	
	}
	
	def idGen(ActivityScope s)
	{
		var type = type(s)	
		var id = id(s)	
		if (shortNames.contains(type+"_"+s.name))
			shortNames.put(type+"_"+id, type+"_"+id)
		else
			shortNames.put(type+"_"+id, type+"_"+s.name)	
	}
	
	def String id2static(String id)
	{
		id.replace(".","_");
	}


	def dispatch String type(Relation r)
	{
		if (r.from.type instanceof ObjectType) 
			"OREF"
		else
			"CREF"
	}
	
	def String type(Action a, ArchElementType pType, String parentName)
	{
		if (type(pType).equals("COMP") || type(pType).equals("CONN")) 
			"CACTION_"+parentName
		else
			"OACTION_"+parentName
	}
	
	
	def dispatch String type(HadlStructure t) {
		"STRUCT"
	}
	
	def dispatch String type(ActivityScope t) {
		"SCOPE"
	}
	
	def dispatch String type(CollabLink t) {
		"LINK"
	}
	
	def dispatch String type(RequestType t) {
		"REQ"
	}

	def dispatch String type(StreamType t) {
		"STM"
	}

	def dispatch String type(ArtifactType t) {
		"ART"
	}

	def dispatch String type(MessageType t) {
		"MSG"
	}

	def dispatch String type(ComponentType t) {
		"COMP"
	}
	
	def dispatch String type(ConnectorType t) {
		"CONN"
	}	

	def dispatch String id(HadlModel o) {
		modelId // check whether we need to include the package name into a FQN	
	}

	def dispatch String id(ActivityScope o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(Action o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(ArchElement o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(CollabLink o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String id(Relation r) {
		id(r.eContainer)+"."+r.name
	}

	def dispatch String id(HadlStructure o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(Wire o) {
		id(o.eContainer)+"."+o.name
	}
}
