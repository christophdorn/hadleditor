package at.tuwien.dsg.generator.util;

import at.tuwien.dsg.dsl.Binding;
import at.tuwien.dsg.dsl.BindingType;
import at.tuwien.dsg.dsl.DslFactory;
import at.tuwien.dsg.dsl.Step;
import at.tuwien.dsg.dsl.StepInterface;

public class ImplicitAgentBindingFactory {

	private ImplicitAgentBindingFactory() {

	}

	/**
	 * If no agent binding is defined for step s, createAgentBinding(s) returns
	 * a implicit agent binding from parent:agent to s:agent.
	 * 
	 * @param s
	 * @return agent binding or null
	 */
	public static void createAgentBinding(Step s) {
		if (isNotRoot(s) && hasNoAgentBinding(s)) {
			Binding b = DslFactory.eINSTANCE.createBinding();
			b.setBindingType(DslFactory.eINSTANCE.createInBinding());			
			//TODO: obtain local agent and parent agent to create binding
			b.setLeft(getAgentResourceFromStep((Step)s.eContainer()));
			b.setRight(getAgentResourceFromStep(s));
			s.getBindings().add(b);
		}
	}
	
	private static StepInterface getAgentResourceFromStep(Step s)
	{
		for(StepInterface si : s.getInterfaces()){
			if(si.getName().trim().equalsIgnoreCase("agent")){
				return si;
			}
		}
		return null;
	}


	private static boolean isNotRoot(Step s) {
		return s.eContainer() != null && s.eContainer() instanceof Step;
	}

	/**
	 * checks if there is an InBinding or an InOutBinding with the name agent.
	 * child-name is always on the left side of the binding
	 * @param s
	 * @return
	 */
	private static boolean hasNoAgentBinding(Step s) {
		for(Binding b : s.getBindings()){
			if(b.getLeft().getName().trim().equalsIgnoreCase("agent")){
				return false;
			}
		}
		return true;
	}
}
