package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ActivityScope
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.Contains
import at.tuwien.dsg.dsl.Depends
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.Inherits
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.ObjectType
import at.tuwien.dsg.dsl.Primitive
import at.tuwien.dsg.dsl.References
import at.tuwien.dsg.dsl.Relation
import at.tuwien.dsg.dsl.RequestType
import at.tuwien.dsg.dsl.StreamType
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.Wire
import at.tuwien.dsg.dsl.SubStructure
import at.tuwien.dsg.generator.util.IdProvider
import at.tuwien.dsg.dsl.CollaboratorType

/**
 * hadl generator
 */
class HadlGenerator implements IGenerator {
	var String modelId
	var String packageId
	
	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				packageId = m.package
			} else {
				packageId = m.hashCode.toString
			}
			if (m.hadl != null)
				modelId = m.hadl.name
			fsa.generateFile(packageId + "-hADL.xml", m.compile)			
		}

	}

	def compile(Model m) '''
		«IF m.hadl != null»
			«m.hadl.compile»
		«ENDIF»
	'''

	def compile(HadlModel hm) '''
	<?xml version="1.0" encoding="UTF-8"?>
	<hADLmodel id="«modelId»"
		xmlns="http://at.ac.tuwien.dsg/hADL/hADLcore" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 	
		xmlns:hADLexe="http://at.ac.tuwien.dsg/hADL/hADLexecutable"
		xmlns:xlink="http://www.w3.org/1999/xlink" 
	
		xsi:schemaLocation="http://at.ac.tuwien.dsg/hADL/hADLcore 		../schema/hADLcore.xsd	">
		<name>«hm.name»</name>
		<description />
		<extension />
		«FOR hs : hm.structures»
		«hs.compile»
		«ENDFOR»
	</hADLmodel>
	'''

	def compile(HadlStructure hs) '''
		<hADLstructure id="«id(hs)»">
			    <name>«hs.name»</name>
			    <description />
			    <extension />
				«FOR e : hs.elements»
					«IF (e instanceof ArchElement) && (e as ArchElement).type instanceof ComponentType »
				«(e as ArchElement).compile»
					«ENDIF»
				«ENDFOR»
				«FOR e : hs.elements»
					«IF (e instanceof ArchElement) && (e as ArchElement).type instanceof ConnectorType »
				«(e as ArchElement).compile»
					«ENDIF»
				«ENDFOR»
				«FOR e : hs.elements»
					«IF (e instanceof ArchElement) && (e as ArchElement).type instanceof ObjectType »
			«(e as ArchElement).compile»
					«ENDIF»
				«ENDFOR»
				«FOR e : hs.elements»
					«IF e instanceof CollabLink»
						«(e as CollabLink).compile»
					«ENDIF»
				«ENDFOR»
				«FOR r : hs.relations»
					«r.compileObj»
				«ENDFOR»
				«FOR r : hs.relations»
					«r.compileCollab»
				«ENDFOR»
				«FOR c : hs.activityScopes»
					«c.compile»
				«ENDFOR»
		</hADLstructure>
	'''

	def compile(ActivityScope ac) '''
		<activityScope id="«id(ac)»">
			<name>«ac.name»</name>
			<cardinality lowerBound="«ac.cardinality.lowerBound»" upperBound="«ac.cardinality.upperBound»"/>			
			<collaboratorRef>
			«FOR or : ac.objectRefs» «IF or.type instanceof CollaboratorType»
				«id(or)» «ENDIF»
			«ENDFOR»
			</collaboratorRef>
			<objectRef>
			«FOR or : ac.objectRefs» «IF or.type instanceof ObjectType»
				«id(or)» «ENDIF»
			«ENDFOR»
			</objectRef>			
			<linkRef>«FOR lr : ac.linkRefs SEPARATOR ' '»«id(lr)»«ENDFOR»
			</linkRef>
		</activityScope>
	'''

	def compile(CollabLink l) '''
		«val objAE = l.objActionEndpoint»
		«val collAE = l.collabActionEndpoint»
		«IF l.name != null»
		<link id="«id(l)»">
		«ELSE»
		<link>
		«ENDIF»
				<name>«l.name»</name>
				<objActionEndpoint>«id(objAE)»</objActionEndpoint>
				<collabActionEndpoint>«id(collAE)»</collabActionEndpoint>
		</link>
	'''

	def compile(ArchElement ae) '''
		«startTag(ae.type)»id="«id(ae)»"«typeAttribute(ae)»>
			<name>«ae.name»</name>
			<description />
			<extension />
				«FOR a : ae.actions»
					«a.compile»
				«ENDFOR»
				«IF ae.substructure != null»
					«ae.substructure.compile»
				«ENDIF»
		«endTag(ae.type)»
	'''

	def compile(Action a) '''
		<action id="«id(a)»">
		   <name>«a.name»</name>
		   <description />
		   <extension />
		   «FOR p : a.types»
		   	«primitive(p)»
		   «ENDFOR»
		   «IF a.cardinality != null»
		   	<cardinality lowerBound="«a.cardinality.lowerBound»" upperBound="«a.cardinality.upperBound»" />
		   «ENDIF»
		 </action>
	'''

	def compileObj(Relation r) '''
		«IF r.from.type instanceof ObjectType»
		<objectRef id="«id(r)»" xsi:type="tObjectCompositionRef">
			<name>«r.name»</name>
			<objFrom>«id(r.from)»</objFrom>
			<objTo>«id(r.to)»</objTo>
			<compositionType>«relationType(r.type)»</compositionType>
		</objectRef>
		«ENDIF»
	'''

	def compileCollab(Relation r) '''
		«IF r.from.type instanceof CollaboratorType»
		<collabRef id="«id(r)»" xsi:type="tCollabCompositionRef">
			<name>«r.name»</name>
			<compconnFrom>«id(r.from)»</compconnFrom>
			<compconnTo>«id(r.to)»</compconnTo>
			<compositionType>«relationType(r.type)»</compositionType>
		</collabRef>
		«ENDIF»
	'''

	def compile(SubStructure sub) '''
		«IF sub.isVirtual == null»
			<substructure>
		«ELSE»
			<substructure isVirtual="«sub.isVirtual»">
		«ENDIF»
			«FOR w : sub.wires»
			«w.compile»
			«ENDFOR»
			<substructureRef>«id(sub.structureRef)»</substructureRef>
		</substructure>
	'''

	def compile(Wire w) '''
		«val objAE = w.objActionEndpoint»
		«val collAE = w.collabActionEndpoint»
				
		«IF w.name != null»
		<substructureWire id="«id(w)»">
			<name>«w.name»</name>
		«ELSE»
		<substructureWire id="«id(w.eContainer)».«IdProvider.generatedWireId»">
			<name/>
		«ENDIF»
			<objActionEndpoint>«id(objAE)»</objActionEndpoint>
			<collabActionEndpoint>«id(collAE)»</collabActionEndpoint>
		</substructureWire>
	'''

	def String primitive(Primitive p) {
		switch p {
			case C: '<primitive>CREATE</primitive>'
			case R: '<primitive>READ</primitive>'
			case U: '<primitive>UPDATE</primitive>'
			case D: '<primitive>DELETE</primitive>'
			default: ""
		}
	}

	/**
	 * type attribute nur bei konkreter Klasse tSimpleCollabObject gefunden -> somit nur für ObjectType relevant
	 */
	def typeAttribute(ArchElement ae) {
		if (ae.type instanceof ObjectType) {
			val t = ae.type as ObjectType
			return " type=\"" + xsdType(t) + "\" xsi:type=\"tSimpleCollabObject\""
		}
		return ""
	}

	def dispatch String xsdType(RequestType t) {
		"REQUEST"
	}

	def dispatch String xsdType(StreamType t) {
		"STREAM"
	}

	def dispatch String xsdType(ArtifactType t) {
		"ARTIFACT"
	}

	def dispatch String xsdType(MessageType t) {
		"MESSAGE"
	}

	def dispatch String startTag(ComponentType t) {
		"<component "
	}

	def dispatch String startTag(RequestType t) {
		"<object "
	}

	def dispatch String startTag(StreamType t) {
		"<object "
	}

	def dispatch String startTag(ArtifactType t) {
		"<object "
	}

	def dispatch String startTag(MessageType t) {
		"<object "
	}

	def dispatch String startTag(ConnectorType t) {
		"<connector "
	}

	def dispatch String endTag(ComponentType t) {
		"</component>"
	}

	def dispatch String endTag(RequestType t) {
		"</object>"
	}

	def dispatch String endTag(StreamType t) {
		"</object>"
	}

	def dispatch String endTag(ArtifactType t) {
		"</object>"
	}

	def dispatch String endTag(MessageType t) {
		"</object>"
	}

	def dispatch String endTag(ConnectorType t) {
		"</connector>"
	}

	def dispatch String relationType(Inherits r) {
		"TO_INHERITS_FROM"
	}

	def dispatch String relationType(Contains r) {
		"FROM_CONTAINS_TO"
	}

	def dispatch String relationType(Depends r) {
		"FROM_DEPENDS_ON_TO"
	}

	def dispatch String relationType(References r) {
		"FROM_REFERENCES_TO"
	}

	def dispatch String id(HadlModel o) {
		modelId // check whether we need to include the package name into a FQN	
	}

	def dispatch String id(ActivityScope o) {
		id(o.eContainer)+"."+o.name
	}


	def dispatch String id(Action o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(ArchElement o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(CollabLink o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String id(Relation r) {
		id(r.eContainer)+"."+r.name
	}

	def dispatch String id(HadlStructure o) {
		id(o.eContainer)+"."+o.name
	}

	def dispatch String id(Wire o) {
		id(o.eContainer)+"."+o.name
	}
	
	def dispatch String id(SubStructure o)
	{
		id(o.structureRef)
	}
}
