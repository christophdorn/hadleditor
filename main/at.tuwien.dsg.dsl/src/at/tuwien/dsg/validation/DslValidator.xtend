package at.tuwien.dsg.validation

import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.Binding
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.CollaboratorType
import at.tuwien.dsg.dsl.DslPackage
import at.tuwien.dsg.dsl.HadlElement
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.LittleJilModel
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.ObjectType
import at.tuwien.dsg.dsl.Primitive
import at.tuwien.dsg.dsl.Relation
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepInterface
import at.tuwien.dsg.dsl.SubStructure
import at.tuwien.dsg.dsl.Wire
import at.tuwien.dsg.generator.util.ImplicitInterfaceFactory
import java.util.HashSet
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.CheckType

/**
 * Custom validation rules. 
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class DslValidator extends AbstractDslValidator {

//	private final static Logger LOG = Logger.getLogger(DslValidator);
	public static val DUPLICATE_PRIMITIVE = 'duplicate primitive'
	public static val INSUFFICIENT_PRIMITIVES = 'insufficient Primitives'
	public static val OVERPRIVISIONED_PRIMITIVES = 'overprovisioned Primitives'
	public static val MISSING_AGENT = "missing agent"
	public static val WRONG_ENDPOINT_TYPE = "wrong endpoint type"
	public static val TOO_MANY_MODELS = "to many models"
	public static val TOO_MANY_HADL_MODELS = "to many hADL models"
	public static val TOO_MANY_LJ_MODELS = "to many little-jil models"
	public static val OBJTO_WRONG_TYPE = "objTo wrong type"
	public static val OBJFROM_WRONG_TYPE = "objFrom wrong type"
	public static val UNEQ_RELATION_TYPE = "unequal relation type"
	public static val WRONG_WIRE_ACTION_TYPE = "wrong wire action type"
	public static val WIRE_ACTION_OUT_OF_SCOPE = "wire action out of scope"
	public static val ENUM2TYPE_MAP = #{ArchElementType.OBJECT -> ObjectType,
		ArchElementType.COLLABORATOR -> CollaboratorType}
	public static val ENUM2MSG_MAP = #{ArchElementType.OBJECT ->
		"referenced action belongs not to an object typed element.",
		ArchElementType.COLLABORATOR -> "referenced action belongs not to an collaborator typed element."}

// no longer needed as enforced by the DSL itself
//	@Check(CheckType::NORMAL)
//	def onlyOneHadlAndOneLittleJilModelPerFile(Model m) {
//		if (m.models.size > 2) {
//			val msg = "There are two many model declared. Only one Little-Jil and one hADL model are allowed per file!"
//			error(msg, DslPackage.Literals.MODEL__MODELS, TOO_MANY_MODELS, "model")
//		} else if (m.models.size == 2) {
//			val m1 = m.models.get(0)
//			val m2 = m.models.get(1)
//			if (m1 instanceof HadlModel && m2 instanceof HadlModel) {
//				val msg = "There are two hADL models defined. Only one hADL model is allowed per file!"
//				error(msg, DslPackage.Literals.MODEL__MODELS, TOO_MANY_HADL_MODELS, "model")
//			}
//			if (m1 instanceof LittleJilModel && m2 instanceof LittleJilModel) {
//				val msg = "There are two little-jil models defined. Only one little-jil model is allowed per file!"
//				error(msg, DslPackage.Literals.MODEL__MODELS, TOO_MANY_LJ_MODELS, "model")
//			}
//		}
//	}

	@Check(CheckType::NORMAL)
	/**
	 * LITTLE-JIL
	 * validation rule ensures that every root step has an agent. 
	 * the agent type must be InterfaceType.RESOURCE
	 */
	def agentDefintionInRootStep(Step s) {
		if (s.eContainer != null && s.eContainer instanceof Model) {

			// is a root step, so it needs an interface declaration agent
			for (si : s.interfaces) {
				if (si.name.equalsIgnoreCase("agent") && si.kind == InterfaceType.RESOURCE) {
					return;
				}
			}
			val msg = String.format("Root step %s needs to declare an Resource-interface with the name 'agent'", s.name)
			error(msg, DslPackage.Literals.STEP__INTERFACES, MISSING_AGENT, s.name)
		}
	}

	/**
	 * LITTLE-JIL
	 * if name of the right side of binding does not match any step-interface name in parents,
	 * no type can be derived for implicit step-interface declaration {@see ImplicitInterfaceFactory#getType(EObject eContainer, String name}
	 */
	@Check(CheckType::NORMAL)
	def bindingTypeDerivable(Binding b) {
		val parentStep = b.eContainer.eContainer;
		if (!findMatchingName(b.right.name, parentStep)) {
			val msg = String.format("Could not derive type for binding. Check if name %s exists in parent steps",
				b.right)
			error(msg, b, DslPackage.Literals.BINDING__RIGHT)
		}
	}

//	/**
//	 * checks if wires between actions are compatible. 
//	 */
//	@Check(CheckType::NORMAL)
//	def checkSameActionPrimitive(CollabRef cr) {
//		val fromErrorMsg = 'Requiring Action has subset of primitives from Providing Action'
//		val fromLiteral = DslPackage.Literals::COLLAB_REF__FROM
//		val toErrorMsg = 'Providing Action does not fulfill primitives of Requiring Action'
//		val toLiteral = DslPackage.Literals::COLLAB_REF__TO
//		actionCheck(cr.from.types, cr.to.types, fromErrorMsg, toErrorMsg, fromLiteral, toLiteral, cr.name)
//	}

	/**
	 * checks if links between actions are compatible. 
	 */
	@Check(CheckType::NORMAL)
	def checkSameActionPrimitive(CollabLink link) {
		val collabActionErrorMsg = 'Collaborator Action has subset of primitives from Object Action'
		val collabLiteral = DslPackage.Literals::COLLAB_LINK__COLLAB_ACTION_ENDPOINT
		val objActionErrorMsg = 'Object Action has subset of primitives from Collaborator Action'
		val objLiteral = DslPackage.Literals::COLLAB_LINK__OBJ_ACTION_ENDPOINT

		actionCheck(link.collabActionEndpoint.types, link.objActionEndpoint.types, collabActionErrorMsg,
			objActionErrorMsg, collabLiteral, objLiteral, link.name)

		val collaborator = link.collabActionEndpoint.eContainer as ArchElement
		val object = link.objActionEndpoint.eContainer as ArchElement
		if (!(collaborator.type instanceof CollaboratorType)) {
			warning("Element of referenced collabActionEndpoint is not a CollaboratorType",
				DslPackage.Literals::COLLAB_LINK__COLLAB_ACTION_ENDPOINT, DslValidator.WRONG_ENDPOINT_TYPE, link.name)
		}
		if (!(object.type instanceof ObjectType)) {
			warning("Element of referenced objActionEndpoint is not a ObjectType",
				DslPackage.Literals::COLLAB_LINK__OBJ_ACTION_ENDPOINT, DslValidator.WRONG_ENDPOINT_TYPE, link.name)
		}
	}

	def actionCheck(EList<Primitive> fromTypes, EList<Primitive> toTypes, String fromErrorMsg, String toErrorMsg,
		EReference fromLiteral, EReference toLiteral, String name) {
		val isSubsetFrom = fromTypes.containsAll(toTypes);
		val isSubsetTo = toTypes.containsAll(fromTypes);
		if (isSubsetTo && !isSubsetFrom) {
			warning(fromErrorMsg, fromLiteral, OVERPRIVISIONED_PRIMITIVES, name)
		} else if (!isSubsetTo && ! isSubsetFrom) {
			warning(toErrorMsg, toLiteral, INSUFFICIENT_PRIMITIVES, name)
		}
	}

	/**
	 * checks if a primitive in an actions is used more than once
	 */
	@Check(CheckType::FAST)
	def duplicatePrimitives(Action a) {

		// for each name store the element of its first occurrence
		if (a.types != null && !a.types.empty) {

			// the set of duplicate names
			var duplicates = new HashSet<Primitive>();

			// iterate (once!) over all types in the model
			for (p : a.types) {
				if (!duplicates.add(p)) // duplicate	
					error("Duplicate Primitive " + p, a, DslPackage.Literals.ACTION__TYPES, DUPLICATE_PRIMITIVE);
			}
		}
	}

	def findMatchingName(String name, EObject pstep) {
		var step = pstep;
		while (step != null && step instanceof Step) {
			for (StepInterface si : (step as Step).interfaces) {
				if (si.name.equals(name)) {
					return true;
				}
			}
			for (Binding b : (step as Step).bindings) {
				if (b.left.equals(name)) {
					return true;
				}
			}
			step = step.eContainer;
		}
		return false
	}

	/**
	 * checks if relation is only between objects and that they have the same type
	 */
	@Check(CheckType::NORMAL)
	def checkRelation(Relation r) {
		val to = r.to as ArchElement
		val from = r.from as ArchElement
		if ((to.type instanceof ObjectType) && !(from.type instanceof ObjectType)) {
			error("If TO reference is of Object subtype, then also FROM reference must be of Object subtype.", 
				DslPackage.Literals::RELATION__TO, DslValidator.UNEQ_RELATION_TYPE, r.name)
		}
		if ((to.type instanceof CollaboratorType) && !(from.type instanceof CollaboratorType)) {
			error("If TO reference is of Collaborator subtype, then also FROM reference must be of Collaborator subtype.", 
				DslPackage.Literals::RELATION__TO, DslValidator.UNEQ_RELATION_TYPE, r.name)
		}
//		if (from.type.eClass != to.type.eClass) {
//			warning(
//				"Relations are only allowed between objects of the same type. Referenced objects are not from the same type.",
//				DslPackage.Literals::RELATION__NAME, DslValidator.UNEQ_RELATION_TYPE, r.name)
//		}
	}

	@Check(CheckType::NORMAL)
	def checkActionReferences(Wire w) {
		val refType = getEnumTypeOfArchElementWhichContainsWire(w)
		checkIfActionBelongsToObject(w.collabActionEndpoint, refType, DslPackage.Literals::WIRE__COLLAB_ACTION_ENDPOINT)
		checkIfActionBelongsToObject(w.objActionEndpoint, refType, DslPackage.Literals::WIRE__OBJ_ACTION_ENDPOINT)

		val structureRef = (w.eContainer as SubStructure).structureRef
		var tmp = w as EObject
		while (!(tmp instanceof HadlStructure)) {
			tmp = tmp.eContainer
		}
		val structure = tmp as HadlStructure
		val availableActions = new HashSet<Action>
		availableActions.addAll(getActionInStructure(structureRef, refType))
		availableActions.addAll(getActionInStructure(structure, refType))

		if (!availableActions.contains(w.objActionEndpoint)) {
			val msg = String.format(
				"referenced action %s is not in scope. Only actions in structure %s and structure %s are valid.",
				w.objActionEndpoint.name, structure.name, structureRef.name)
			warning(msg, DslPackage.Literals::WIRE__OBJ_ACTION_ENDPOINT, DslValidator.WIRE_ACTION_OUT_OF_SCOPE,
				w.objActionEndpoint.name)
		}
		if (!availableActions.contains(w.collabActionEndpoint)) {
			val msg = String.format(
				"referenced action %s is not in scope. Only actions in structure %s and structure %s are valid.",
				w.collabActionEndpoint.name, structure.name, structureRef.name)
			warning(msg, DslPackage.Literals::WIRE__COLLAB_ACTION_ENDPOINT, DslValidator.WIRE_ACTION_OUT_OF_SCOPE,
				w.collabActionEndpoint.name)
		}
	}

	def getEnumTypeOfArchElementWhichContainsWire(Wire wire) {
		val c = (wire.eContainer.eContainer as ArchElement).type.class
		if (ObjectType.isAssignableFrom(c)) {
			return ArchElementType.OBJECT
		} else if (CollaboratorType.isAssignableFrom(c)) {
			return ArchElementType.COLLABORATOR
		}
		// this point should never be reached
		throw new RuntimeException("Unknown ArchElement type!")
	}

	def checkIfActionBelongsToObject(Action action, ArchElementType refType, EReference ref) {
		if (action.eContainer instanceof ArchElement) {
			val archElement = action.eContainer as ArchElement
			val refClass = ENUM2TYPE_MAP.get(refType)
			if (!(refClass.isAssignableFrom(archElement.type.class))) {
				warning(ENUM2MSG_MAP.get(refType), ref, DslValidator.WRONG_WIRE_ACTION_TYPE, action.name)
			}
		}
	}

	def getActionInStructure(HadlStructure structure, ArchElementType refType) {
		val result = new HashSet<Action>
		for (HadlElement he : structure.elements) {
			if (he instanceof ArchElement) {
				var ae = he as ArchElement
				val refClass = ENUM2TYPE_MAP.get(refType)
				if (refClass.isAssignableFrom(ae.type.class)) {
					for (Action a : ae.actions) {
						result.add(a)
					}
				}
			}
		}
		result
	}

}
