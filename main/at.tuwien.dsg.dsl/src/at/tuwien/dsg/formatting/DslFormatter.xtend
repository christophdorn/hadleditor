/*
 * generated by Xtext
 */
package at.tuwien.dsg.formatting

import at.tuwien.dsg.services.DslGrammarAccess
import javax.inject.Inject
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig

// import com.google.inject.Inject;
// import at.tuwien.dsg.services.DslGrammarAccess
/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
class DslFormatter extends AbstractDeclarativeFormatter {

	@Inject DslGrammarAccess g

	override protected void configureFormatting(FormattingConfig c) {
		c.setAutoLinewrap(120)
		c.setWrappedLineIndentation(2)

//		c.setLinewrap(1).after(g.modelAccess.modelsKeyword_2)
//		c.setLinewrap(1).before(g.hadlModelAccess.nameKeyword_1)
//		c.setLinewrap(1).before(g.hadlModelAccess.structuresKeyword_3)

		c.setLinewrap(2).after(g.modelAccess.packageAssignment_0_1)

//		c.setLinewrap(2).before(g.modelAccess.importKeyword_1_0)

//		c.setLinewrap(1).before(g.hadlStructureAccess.elementsKeyword_2)
		c.setLinewrap(1).after(g.hadlStructureAccess.elementsAssignment_3)
		c.setLinewrap(1).before(g.hadlStructureAccess.elementsAssignment_3)

//		c.setLinewrap(1).before(g.littleJilModelAccess.nameKeyword_1)
//		c.setLinewrap(1).before(g.littleJilModelAccess.rootstepsKeyword_3)
//
//		c.setLinewrap(1).before(g.archElementAccess.actionsKeyword_3_0)
//		c.setLinewrap(2).after(g.archElementAccess.actionsAssignment_3_1)
//
//		c.setLinewrap(1).before(g.stepAccess.kindAssignment_0)
//		c.setLinewrap(1).before(g.stepAccess.cardinalityKeyword_3_0)
//		c.setLinewrap(1).before(g.stepAccess.handlersKeyword_4_0)
//		c.setLinewrap(1).before(g.stepAccess.interfacesKeyword_5_0)
//		c.setLinewrap(1).before(g.stepAccess.bindingsKeyword_6_0)
//		c.setLinewrap(1).before(g.stepAccess.stepsKeyword_7_0)

		for(p : g.findKeywordPairs("structures", "end")){
			c.setIndentationIncrement().after(p.first);
			c.setIndentationDecrement().before(p.second);
			c.setLinewrap(1).after(p.first)
		}
		
		for(p : g.findKeywordPairs("rootsteps", "end")){
			c.setIndentationIncrement().after(p.first);
			c.setIndentationDecrement().before(p.second);
			c.setLinewrap(1).after(p.first)
		}

		for(p : g.findKeywordPairs("elements", "end")){
			c.setIndentationIncrement().after(p.first);
			c.setIndentationDecrement().before(p.second);
			c.setLinewrap(1).after(p.first)
			c.setLinewrap(1).around(p.second)
		}
		
//		c.setLinewrap(1).after(g.stepAccess.handlersKeyword_4_0)
//		c.setLinewrap(1).after(g.stepAccess.interfacesKeyword_5_0)
		
		for(p : g.findKeywordPairs("[", "]")) {
			c.setIndentationIncrement().before(p.first);
			c.setIndentationDecrement().after(p.second);
            c.setNoSpace().after(p.first)
            c.setNoSpace().before(p.second)
            c.setLinewrap(1).around(p.second)
        }
        
        for(p : g.findKeywordPairs("{", "}")) {
        	c.setIndentationIncrement().before(p.first);
			c.setIndentationDecrement().after(p.second);
            c.setNoSpace().after(p.first)
            c.setNoSpace().before(p.second)
            c.setLinewrap(1).around(p.second)
        }

		for (p : g.findKeywordPairs("(", ")")) {
			c.setIndentationIncrement().after(p.first)
			c.setIndentationDecrement().before(p.second)
			c.setLinewrap(1).around(p.second)
		}

	}
}
