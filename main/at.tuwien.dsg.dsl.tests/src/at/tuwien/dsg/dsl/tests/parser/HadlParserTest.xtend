package at.tuwien.dsg.dsl.tests.parser

import at.tuwien.dsg.DslInjectorProvider
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.Contains
import at.tuwien.dsg.dsl.Depends
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.Inherits
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.Primitive
import at.tuwien.dsg.dsl.References
import at.tuwien.dsg.dsl.StreamType
import javax.inject.Inject
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@InjectWith(DslInjectorProvider)
@RunWith(XtextRunner)
class HadlParserTest {
	@Inject extension ParseHelper<Model>

	@Test
	def void parseSeveralStructures() {
		val model = '''
package at.tuwien.test
models
	( 	name testmodel
		structures
			name structure1
			elements
				Artifact name Artifact1
			end
			name structure2
			elements
				Message name Message1
			end
			name structure3
			elements
				Stream name Stream1
			end
		end
	)
end
'''.
			parse
		Assert::assertEquals(1, model.models.size)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hadlModel.name)
		Assert::assertEquals(3, hadlModel.structures.size)

		val struc1 = hadlModel.structures.get(0)
		Assert::assertEquals("structure1", struc1.name)
		Assert::assertEquals(1, struc1.elements.size)
		Assert::assertTrue(struc1.elements.get(0) instanceof ArchElement)
		val art = struc1.elements.get(0) as ArchElement
		Assert::assertTrue(art.type instanceof ArtifactType)
		Assert::assertEquals("Artifact1", art.name)

		val struc2 = hadlModel.structures.get(1)
		Assert::assertEquals("structure2", struc2.name)
		Assert::assertEquals(1, struc2.elements.size)
		Assert::assertTrue(struc2.elements.get(0) instanceof ArchElement)
		val msg = struc2.elements.get(0) as ArchElement
		Assert::assertTrue(msg.type instanceof MessageType)
		Assert::assertEquals("Message1", msg.name)

		val struct3 = hadlModel.structures.get(2)
		Assert::assertEquals("structure3", struct3.name)
		Assert::assertEquals(1, struct3.elements.size)
		Assert::assertTrue(struct3.elements.get(0) instanceof ArchElement)
		val stream = struct3.elements.get(0) as ArchElement
		Assert::assertTrue(stream.type instanceof StreamType)
		Assert::assertEquals("Stream1", stream.name)
	}

	@Test
	def void parseCollabLink() {
		val model = '''
package at.tuwien.test
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Component name WikiCompositeObject actions
				[EditingWiki type {C R U D}]
				Artifact name WikiPage actions 
				[read]
				Link name Link1 testmodel.teststructure.WikiPage.read : testmodel.teststructure.WikiCompositeObject.EditingWiki
			end
		end
	)
end
'''.
			parse
		Assert::assertEquals(1, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)

		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		Assert::assertTrue(hadlModel.structures.get(0) instanceof HadlStructure)

		val hadlStructure = hadlModel.structures.get(0) as HadlStructure
		Assert::assertEquals("teststructure", hadlStructure.name)

		Assert::assertEquals(3, hadlStructure.elements.size)
		val e1 = hadlStructure.elements.get(0)
		Assert::assertTrue(e1 instanceof ArchElement)

		val e2 = hadlStructure.elements.get(1)
		Assert::assertTrue(e2 instanceof ArchElement)

		val e3 = hadlStructure.elements.get(2)
		Assert::assertTrue(e3 instanceof CollabLink)

		val wikiComp = e1 as ArchElement
		Assert::assertEquals(1, wikiComp.actions.size)
		val editWiki = wikiComp.actions.get(0)

		val wikiPage = e2 as ArchElement
		Assert::assertEquals(1, wikiPage.actions.size)
		val read = wikiPage.actions.get(0)

		val link = e3 as CollabLink
		Assert::assertEquals("Link1", link.name)
		val lref = link.objActionEndpoint
		val rref = link.collabActionEndpoint
		Assert::assertEquals(lref, read)
		Assert::assertEquals(rref, editWiki)
	}

	@Test
	def void parseObjectRef() {
		val model = '''
package at.tuwien.test
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Artifact name WikiPage actions 
				[read]
				Artifact name SubPage
			end
			relations
				[name ParentChild : SubPage inherits from WikiPage]
				[name Inclusion : WikiPage contains SubPage]
				[name Dependency : SubPage depends on WikiPage]
				[name Ref : SubPage references WikiPage]
			end
		end
	)
end'''.
			parse
		Assert::assertEquals(1, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)

		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		Assert::assertTrue(hadlModel.structures.get(0) instanceof HadlStructure)

		val hadlStructure = hadlModel.structures.get(0) as HadlStructure
		Assert::assertEquals("teststructure", hadlStructure.name)

		Assert::assertEquals(2, hadlStructure.elements.size)
		Assert::assertEquals(4, hadlStructure.relations.size)
		val wikiPage = hadlStructure.elements.get(0)
		Assert::assertTrue(wikiPage instanceof ArchElement)
		val subPage = hadlStructure.elements.get(1)
		Assert::assertTrue(subPage instanceof ArchElement)

		var relation = hadlStructure.relations.get(0)
		Assert::assertEquals("ParentChild", relation.name)
		Assert::assertTrue(relation.type instanceof Inherits)
		Assert::assertEquals(wikiPage, relation.objFrom)
		Assert::assertEquals(subPage, relation.objTo)

		relation = hadlStructure.relations.get(1)
		Assert::assertEquals("Inclusion", relation.name)
		Assert::assertTrue(relation.type instanceof Contains)
		Assert::assertEquals(subPage, relation.objFrom)
		Assert::assertEquals(wikiPage, relation.objTo)

		relation = hadlStructure.relations.get(2)
		Assert::assertEquals("Dependency", relation.name)
		Assert::assertTrue(relation.type instanceof Depends)
		Assert::assertEquals(wikiPage, relation.objFrom)
		Assert::assertEquals(subPage, relation.objTo)

		relation = hadlStructure.relations.get(3)
		Assert::assertEquals("Ref", relation.name)
		Assert::assertTrue(relation.type instanceof References)
		Assert::assertEquals(wikiPage, relation.objFrom)
		Assert::assertEquals(subPage, relation.objTo)
	}

	@Test
	def void parseElements() {
		val model = '''
package at.tuwien.test
models
	( 	name testmodel
		structures
			name teststructure
			elements 
				Component name Comp1 actions
					[a1 type {R}]
				Artifact name Obj1 actions
					[b1 type {R}]
					[a2 type {C R}] 
					[a3 type {C R U D}] 
				Connector name Con1 actions
					[c1 type {D}]
			end
		end
	)
end'''.
			parse
		Assert::assertEquals(1, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)

		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		Assert::assertTrue(hadlModel.structures.get(0) instanceof HadlStructure)

		val hadlStructure = hadlModel.structures.get(0) as HadlStructure
		Assert::assertEquals("teststructure", hadlStructure.name)

		Assert::assertEquals(3, hadlStructure.elements.size)
		val componentElement = hadlStructure.elements.get(0)
		Assert::assertTrue(componentElement instanceof ArchElement)
		val component = componentElement as ArchElement
		Assert::assertEquals("Comp1", component.name)
		Assert::assertTrue(component.type instanceof ComponentType)
		Assert::assertEquals(1, component.actions.size)
		Assert::assertEquals("a1", component.actions.get(0).name)
		Assert::assertEquals(1, component.actions.get(0).types.size)
		Assert::assertEquals(Primitive.R, component.actions.get(0).types.get(0))

		val objectElement = hadlStructure.elements.get(1)
		Assert::assertTrue(objectElement instanceof ArchElement)
		val object = objectElement as ArchElement
		Assert::assertEquals("Obj1", objectElement.name)
		Assert::assertTrue(object.type instanceof ArtifactType)
		Assert::assertEquals(3, object.actions.size)

		Assert::assertEquals("b1", object.actions.get(0).name)
		Assert::assertEquals(1, object.actions.get(0).types.size)
		Assert::assertEquals(Primitive.R, object.actions.get(0).types.get(0))

		Assert::assertEquals("a2", object.actions.get(1).name)
		Assert::assertEquals(2, object.actions.get(1).types.size)
		Assert::assertEquals(Primitive.C, object.actions.get(1).types.get(0))
		Assert::assertEquals(Primitive.R, object.actions.get(1).types.get(1))

		Assert::assertEquals("a3", object.actions.get(2).name)
		Assert::assertEquals(4, object.actions.get(2).types.size)
		Assert::assertEquals(Primitive.C, object.actions.get(2).types.get(0))
		Assert::assertEquals(Primitive.R, object.actions.get(2).types.get(1))
		Assert::assertEquals(Primitive.U, object.actions.get(2).types.get(2))
		Assert::assertEquals(Primitive.D, object.actions.get(2).types.get(3))

		val connectorElement = hadlStructure.elements.get(2)
		Assert::assertTrue(connectorElement instanceof ArchElement)
		val connector = connectorElement as ArchElement
		Assert::assertEquals("Con1", connector.name)
		Assert::assertTrue(connector.type instanceof ConnectorType)
		Assert::assertEquals(1, connector.actions.size)
		Assert::assertEquals("c1", connector.actions.get(0).name)
		Assert::assertEquals(1, connector.actions.get(0).types.size)
		Assert::assertEquals(Primitive.D, connector.actions.get(0).types.get(0))
	}

	@Test
	def void parseWires() {
		val model = '''
package at.tuwien.test
models
	( 	name testmodel
		structures
			name teststructure
			elements 
					Component name Comp1 actions
						[a1 type {R}]
					Artifact name Obj1 actions
						[b1 type {R}]
						[a2 type {C R}] 
						[a3 type {C R U D}] 
			end
			collabRefs
			 [name collabRef1: testmodel.teststructure.Comp1.a1 => testmodel.teststructure.Obj1.b1]
			end
		end
	)
end
		'''.
			parse
		Assert::assertEquals(1, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)

		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		Assert::assertTrue(hadlModel.structures.get(0) instanceof HadlStructure)

		val hadlStructure = hadlModel.structures.get(0) as HadlStructure
		Assert::assertEquals("teststructure", hadlStructure.name)

		Assert::assertEquals(2, hadlStructure.elements.size)
		val compElement = hadlStructure.elements.get(0)
		Assert::assertTrue(compElement instanceof ArchElement)
		val comp = compElement as ArchElement
		val compA1 = comp.actions.get(0)
		val objElement = hadlStructure.elements.get(1)
		val obj = objElement as ArchElement
		val objA1 = obj.actions.get(0)

		Assert::assertEquals(1, hadlStructure.collabRefs.size)
		val collabRef = hadlStructure.collabRefs.get(0)
		Assert::assertEquals("collabRef1", collabRef.name)
		Assert::assertEquals(compA1, collabRef.from)
		Assert::assertEquals(objA1, collabRef.to)
	}

	@Test
	def void parseActionCardinality() {
		val model = '''
package at.tuwien.test
models
	( 	name testmodel
		structures
			name teststructure
			elements 
					Artifact name Obj1 actions
						[a1 type {R} cardinality 0,1]
						[a2 type {C R} cardinality 1,*] 
						[a3 type {C R U D} cardinality 0,*] 
			end
		end
	)
end
		'''.
			parse
		Assert::assertEquals(1, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)

		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		Assert::assertTrue(hadlModel.structures.get(0) instanceof HadlStructure)

		val hadlStructure = hadlModel.structures.get(0) as HadlStructure
		Assert::assertEquals("teststructure", hadlStructure.name)

		Assert::assertEquals(1, hadlStructure.elements.size)
		Assert::assertTrue(hadlStructure.elements.get(0) instanceof ArchElement)
		val obj1 = hadlStructure.elements.get(0) as ArchElement
		Assert::assertEquals(3, obj1.actions.size)
		val a1 = obj1.actions.get(0)
		Assert::assertNotNull(a1.cardinality)
		Assert::assertEquals("0", a1.cardinality.lowerBound)
		Assert::assertEquals("1", a1.cardinality.upperBound)

		val a2 = obj1.actions.get(1)
		Assert::assertNotNull(a2.cardinality)
		Assert::assertEquals("1", a2.cardinality.lowerBound)
		Assert::assertEquals("*", a2.cardinality.upperBound)

		val a3 = obj1.actions.get(2)
		Assert::assertNotNull(a3.cardinality)
		Assert::assertEquals("0", a3.cardinality.lowerBound)
		Assert::assertEquals("*", a3.cardinality.upperBound)
	}

	@Test
	def void parseActivityScope() {
		val model = '''
package activityScope
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Component name WikiComponent actions
				[EditingWiki type {C R U D}]
				Artifact name WikiPage actions 
				[read]
				Artifact name WikiSubPage actions 
				[read]
				Link name Link1 testmodel.teststructure.WikiPage.read : testmodel.teststructure.WikiComponent.EditingWiki
				Link name Link2 testmodel.teststructure.WikiSubPage.read : testmodel.teststructure.WikiComponent.EditingWiki
				
			end
			activityScopes
				[	links 
						testmodel.teststructure.Link1
					collaborators
						testmodel.teststructure.WikiComponent
					objects
						testmodel.teststructure.WikiPage
					cardinality 1,2
				]
				[	links 
						testmodel.teststructure.Link1
						testmodel.teststructure.Link2
					collaborators
						testmodel.teststructure.WikiComponent
					objects
						testmodel.teststructure.WikiPage
						testmodel.teststructure.WikiSubPage
					cardinality 0,*
				]
			end
		end
	)
end
'''.
			parse
		Assert::assertEquals(1, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hADLModel = model.models.get(0) as HadlModel
		Assert::assertEquals("testmodel", hADLModel.name)
		Assert::assertEquals(1, hADLModel.structures.size)
		Assert::assertTrue(hADLModel.structures.get(0) instanceof HadlStructure)
		val hADLStruct = hADLModel.structures.get(0) as HadlStructure
		Assert::assertEquals("teststructure", hADLStruct.name)

		Assert::assertEquals(5, hADLStruct.elements.size)
		val wikiComponent = hADLStruct.elements.get(0)
		Assert::assertEquals("WikiComponent", wikiComponent.name)
		val wikiPage = hADLStruct.elements.get(1)
		Assert::assertEquals("WikiPage", wikiPage.name)
		val wikiSubPage = hADLStruct.elements.get(2)
		Assert::assertEquals("WikiSubPage", wikiSubPage.name)
		val link1 = hADLStruct.elements.get(3)
		Assert::assertEquals("Link1", link1.name)
		val link2 = hADLStruct.elements.get(4)
		Assert::assertEquals("Link2", link2.name)

		Assert::assertEquals(2, hADLStruct.activityScopes.size)
		val activityScope1 = hADLStruct.activityScopes.get(0)
		Assert::assertEquals(1, activityScope1.linkRefs.size)
		Assert::assertEquals(link1, activityScope1.linkRefs.get(0))
		Assert::assertEquals(1, activityScope1.collaboratorRefs.size)
		Assert::assertEquals(wikiComponent, activityScope1.collaboratorRefs.get(0))
		Assert::assertEquals(1, activityScope1.objectRefs.size)
		Assert::assertEquals(wikiPage, activityScope1.objectRefs.get(0))
		val cardinality1 = activityScope1.cardinality
		Assert::assertEquals("1", cardinality1.lowerBound)
		Assert::assertEquals("2", cardinality1.upperBound)

		val activityScope2 = hADLStruct.activityScopes.get(1)
		Assert::assertEquals(2, activityScope2.linkRefs.size)
		Assert::assertEquals(link1, activityScope2.linkRefs.get(0))
		Assert::assertEquals(link2, activityScope2.linkRefs.get(1))
		Assert::assertEquals(1, activityScope2.collaboratorRefs.size)
		Assert::assertEquals(wikiComponent, activityScope2.collaboratorRefs.get(0))
		Assert::assertEquals(2, activityScope2.objectRefs.size)
		Assert::assertEquals(wikiPage, activityScope2.objectRefs.get(0))
		Assert::assertEquals(wikiSubPage, activityScope2.objectRefs.get(1))
		val cardinality2 = activityScope2.cardinality
		Assert::assertEquals("0", cardinality2.lowerBound)
		Assert::assertEquals("*", cardinality2.upperBound)
	}

	@Test
	def void parseSubStructure() {
		val model = '''
package substructure
models
	( 	name testmodel
		structures
			name mediawiki
			elements 
				Artifact name WikiPage actions
					[page_read]
					[page_edit]
			end
			
			name VirtualWiki
			elements
				Artifact name VWiki actions
					[virtual_wikireading]
					[virtual_wikiediting]
					substructure reference testmodel.mediawiki virtual true
					wires
						Wire testmodel.mediawiki.WikiPage.page_read : testmodel.VirtualWiki.VWiki.virtual_wikireading
						Wire testmodel.mediawiki.WikiPage.page_edit : testmodel.VirtualWiki.VWiki.virtual_wikiediting
			end
		end
	)
end
		'''.
			parse
			Assert::assertEquals(1, model.models.size)
			Assert::assertTrue(model.models.get(0) instanceof HadlModel)
			val testmodel = model.models.get(0) as HadlModel
			Assert::assertEquals(2, testmodel.structures.size)
			
			val mediaWiki = testmodel.structures.get(0)
			Assert::assertEquals(1, mediaWiki.elements.size)
			Assert::assertTrue(mediaWiki.elements.get(0) instanceof ArchElement)
			val wikiPage = mediaWiki.elements.get(0) as ArchElement
			Assert::assertEquals(2, wikiPage.actions.size)
			val pageRead = wikiPage.actions.get(0)
			val pageEdit = wikiPage.actions.get(1)
			
			val virutalWiki = testmodel.structures.get(1)
			Assert::assertEquals(1, virutalWiki.elements.size)
			Assert::assertTrue(virutalWiki.elements.get(0) instanceof ArchElement)
			val vWiki = virutalWiki.elements.get(0) as ArchElement
			Assert::assertEquals(2, vWiki.actions.size)
			val vRead = vWiki.actions.get(0)
			val vEdit = vWiki.actions.get(1)
			
			Assert::assertNotNull(vWiki.substructure)
			val substruct = vWiki.substructure
			Assert::assertEquals(mediaWiki, substruct.structureRef)
			Assert::assertEquals("true", substruct.isVirtual)
			Assert::assertEquals(2, substruct.wires.size)
			val wire1 = substruct.wires.get(0)
			val wire2 = substruct.wires.get(1)
			Assert::assertEquals(pageRead, wire1.objActionEndpoint)
			Assert::assertEquals(vRead, wire1.collabActionEndpoint)
			Assert::assertEquals(pageEdit, wire2.objActionEndpoint)
			Assert::assertEquals(vEdit, wire2.collabActionEndpoint)
	}

}
