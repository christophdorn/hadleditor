package at.tuwien.dsg.dsl.tests.generator.util;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class DslXmlFormatter {

	private final static Logger LOG = Logger.getLogger(DslXmlFormatter.class);

	public static String prettyFormat(String input) {
		try {
			// remove tabs, newline and whitespaces between tags so formatting
			// works
			String content = input.replaceAll("\\t", "")
					.replaceAll("\\n", "")
					.replaceAll(">\\s+<", "><")
					.replaceAll("id=\"_\\d+\"", "id=\"#\"")
					.replaceAll("id=\"structure_\\d+\"", "id=\"#\"")
					.replaceAll("id=\"\\w+\\.wire\\._\\d+\"", "id=\"#\"");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			// stop the network loading of DTD files
			dbf.setValidating(false);
			dbf.setNamespaceAware(true);
			dbf.setFeature("http://xml.org/sax/features/namespaces", false);
			dbf.setFeature("http://xml.org/sax/features/validation", false);
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(content)));

			// Source xmlInput = new StreamSource(new StringReader(input));
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", 4);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(new DOMSource(doc.getDocumentElement()), xmlOutput);
			return xmlOutput.getWriter().toString().trim();
		} catch (Exception e) {
			LOG.error("Could not format generated ouput!");
			throw new RuntimeException(e);
		}
	}
}
