package at.tuwien.dsg.dsl.tests.generator

import org.eclipse.xtext.junit4.InjectWith
import org.junit.runner.RunWith
import at.tuwien.dsg.DslInjectorProvider
import org.eclipse.xtext.junit4.XtextRunner
import javax.inject.Inject
import org.eclipse.xtext.junit4.util.ParseHelper
import at.tuwien.dsg.dsl.Model
import org.junit.Test
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.FileSystems
import at.tuwien.dsg.dsl.HadlModel
import org.junit.Assert

@InjectWith(DslInjectorProvider)
@RunWith(XtextRunner)
class PlantUMLComponentDiagramTest {
	@Inject extension ParseHelper<Model>	
	
	
	@Test
	def void parseFromFileSystem()
	{
		val path = "D:\\Work\\Workspace\\runtime-EclipseXtext\\coretest\\";
		val filepath = FileSystems.getDefault().getPath(path, "h2lj-test.dsl");
		val content = new String(Files.readAllBytes(filepath), StandardCharsets.UTF_8);
		val model = content.parse
		
		Assert::assertEquals(1, model.models.size)
		val hadlModel = model.models.get(0) as HadlModel

		
	}
}